import { useSelector } from "react-redux";
import style from './log.module.scss';

export default function Log () {
    const log = useSelector((state) => state.timer.log);
    
    function formatTime(date) {
        const options = { hour: '2-digit', minute: '2-digit', second: '2-digit' };
        return date.toLocaleTimeString(undefined, options);                 
    };

    return (
        <>
            <ul className={style.log}>
            {log.map((entry, index) => (
                <li key={index}>
                    Button №{entry.button}: {formatTime(entry.log)} - {formatTime(entry.click)} ({entry.passed} sec)
                </li>
            ))}
            </ul>
        </>
    );
}