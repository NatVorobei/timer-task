import { useDispatch } from 'react-redux';
import Button from '../Button';
import Log from '../Log';
import styles from './timer.module.scss';
import { useState } from 'react';
import { addToLog, clearLog, startTimer } from '../../redux/action/timer';

export default function Timer() {
    const dispatch = useDispatch();
    const [activeButton, setActiveButton] = useState(null);

    function handleButtonClick (buttonText){
        const startClickTime = Date.now();
        dispatch(startTimer(buttonText));
        setActiveButton(buttonText);
        setTimeout(() => {
          const endTime = Date.now();
          const passedTime = (endTime - startClickTime) / 1000; 
          dispatch(addToLog(buttonText, new Date(startClickTime), new Date(endTime), passedTime)); 
          setActiveButton(null);
        }, parseInt(buttonText) * 1000);
    };

    function handleClearClick() {
        dispatch(clearLog());
    };
    return (
        <div className={styles.timer}>
            <div className={styles.timer__container}>
                <div className={styles.timer__container_buttons}>
                    <Button text='1 sec' className={styles.timer__btn} onClick={() => handleButtonClick('1')}/>
                    <Button text='2 sec' className={styles.timer__btn} onClick={() => handleButtonClick('2')}/>
                    <Button text='3 sec' className={styles.timer__btn} onClick={() => handleButtonClick('3')}/>
                    <Button text='Clear' className={styles.timer__btn} onClick={handleClearClick}/>
                </div>
                <div className={styles.timer__container_output}>
                    <Log />
                </div>
            </div>
        </div>
    );
}