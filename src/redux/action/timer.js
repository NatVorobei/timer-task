import { timerTypes } from "../types";

export function startTimer(buttonText) {
    return({
        type: timerTypes.START_TIMER,
        button: buttonText,
    }) 
};

export function addToLog(button, log, click, passed) {
    return({
        type: timerTypes.ADD_TO_LOG,
        button,
        log,
        click,
        passed
    })
};

export function clearLog() {
    return({
        type: timerTypes.CLEAR_LOG
    })
}