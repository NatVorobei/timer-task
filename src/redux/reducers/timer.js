import { timerTypes } from "../types";

const initialState = {
    log: []
};

export function timerReducer (state = initialState, action) {
    switch(action.type) {
        case timerTypes.START_TIMER:
            return {
                ...state,
                button: action.button,
            }
        case timerTypes.ADD_TO_LOG:
            return {
                ...state,
                log: [
                  ...state.log,
                  {
                    button: action.button,
                    log: action.log,
                    click: action.click,
                    passed: action.passed,
                  },
                ],
              };
        case timerTypes.CLEAR_LOG:
            return {
                ...state,
                log: [],
            };
        default:
            return state;
    }
};