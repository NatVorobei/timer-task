export const timerTypes = {
    START_TIMER: 'START_TIMER',
    ADD_TO_LOG: 'ADD_TO_LOG',
    CLEAR_LOG: 'CLEAR_LOG'
};