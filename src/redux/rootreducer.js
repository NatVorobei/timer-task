import { combineReducers } from "redux";
import { timerReducer } from "./reducers/timer";

export const rootReducer = combineReducers({
    timer: timerReducer
});
